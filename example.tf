provider "aws" {
  access_key  = "${var.access_key}"
  secret_key  = "${var.secret_key}"
  region      = "${var.region}"
}

# resource "aws_s3_bucket" "example_movishell_pl" {
#   bucket  = "examplemovishellpl"
#   acl     = "private"
# }

resource "aws_instance" "example" {
  ami           = "${lookup(var.amis, var.region)}"
  instance_type = "t2.micro"
  # depends_on    = ["aws_s3_bucket.example_movishell_pl"]
  # provisioner "local-exec" {
  #   command = "echo ${aws_instance.example.public_ip} > ipaddr.txt"
  # }
}

resource "aws_eip" "ip" {
  instance = "${aws_instance.example.id}"
}
